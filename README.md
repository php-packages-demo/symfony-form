# [symfony/form](https://packagist.org/packages/symfony/form)

[**symfony/form**](https://packagist.org/packages/symfony/form)
![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/form)
Create, process and reuse HTML forms https://symfony.com/form

[[_TOC_]]

# Documentation
# Official documentation
* Symfony5: The Fast Track [*Step 14: Accepting Feedback with Forms*](https://symfony.com/doc/current/the-fast-track/en/14-form.html)
* Inspiration
  * [*Open-Source cross-pollination*
  ](https://symfony.com/blog/open-source-cross-pollination)
  2008-09 Fabien Potencier
    * Django
    * Formencode

# Unofficial documentation
* [*Using Symfony forms with PHP typed properties*
  ](https://www.strangebuzz.com/en/blog/using-symfony-forms-with-php-typed-properties)
  2023-05 COil (Strangebuzz)
* [*Stop Using Entities in Symfony Forms:
  Do you use domain entities in your Symfony Forms? Let‘s transfer data from forms correctly.*
  ](https://blog.devgenius.io/stop-using-entities-in-symfony-forms-c42ec0f5fcf6)
  2022-10 .com software @ Dev Genius
* [*Building a Shopping Cart with Symfony*
  ](https://dev.to/qferrer/introduction-building-a-shopping-cart-with-symfony-f7h)
  (11 Part Series)
  2020-12 Quentin
* [*Symfony Forms in Detail*
  ](https://speakerdeck.com/el_stoffel/symfony-forms-in-detail)
  2019-08 Christopher Hertel
* [*How to handle JSON requests using forms on Symfony 4 and getting a clean code*
  ](https://medium.com/@ideneal/how-to-handle-json-requests-using-forms-on-symfony-4-and-getting-a-clean-code-67dd796f3d2f)
  2019-07
* [*Disable the HTML5 validation of all your Symfony forms with a feature flag*
  ](https://www.strangebuzz.com/en/blog/disable-the-html5-validation-of-all-your-symfony-forms-with-a-feature-flag)
  2019-06

## Debugging
* [*How to render form errors in a list with Twig in Symfony 4*
  ](https://ourcodeworld.com/articles/read/1077/how-to-render-form-errors-in-a-list-with-twig-in-symfony-4)
  2019-11 Carlos Delgado

## Deconstruction
* [*Another way for a Symfony API to ingest POST requests - No Bundle*
  ](https://dev.to/gmorel/another-way-for-a-symfony-api-to-ingest-post-requests-no-bundle-5h15)
  2020-07 Guillaume MOREL

## Loosely coupled application components architectures

### Hexagonal architecture
* (fr) [*Simplifiez vos formulaires Symfony*](https://www.choosit.com/blog/simplifiez-vos-formulaires-symfony/)
  2019-09 Gilles

### Value objects in Domain-driven design
* [*Stop repeating yourself in Doctrine and Symfony forms with Value objects*
  ](https://romaricdrigon.github.io/2019/07/05/value-objects-doctrine-and-symfony-forms)
  2019-07 [Romaric Drigon](https://romaricdrigon.github.io)
* [*ValueObject*](https://martinfowler.com/bliki/ValueObject.html)
  2016-11 [Martin Fowler](https://martinfowler.com/)
* [*Value Objects, Immutable objects and Doctrine Embeddables*
  ](https://io.serendipityhq.com/experience/php-and-doctrine-immutable-objects-value-objects-and-embeddables/)
  2015-07 Aerendir
* [*Value Object*](http://wiki.c2.com/?ValueObject)
* [*Value object*](https://en.m.wikipedia.org/wiki/Value_object)
* [*Domain-driven design*](https://en.m.wikipedia.org/wiki/Domain-driven_design)
